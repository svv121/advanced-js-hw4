"use strict";
/*
AJAX - Asynchronous Javascript and XML. Технологія, яка дозволяє створити запит до сервера, виконати його в фоновому режимі та підвантажувати дані на web-сторінці без перевантаження самої сторінки.
*/

const containerSection = document.querySelector('section.container')
const getUrlOfFilms = (url) => {
    fetch(url)
        .then(res => res.json())
        .then(data => data.forEach(film => {
            const {name, episodeId, openingCrawl, characters} = film;
            containerSection.insertAdjacentHTML("beforeend", `<div>
                <p class="episode">Episode Id: ${episodeId}</p>
                <h2 class="title">${name}</h2>
                <p class="loader" id="dots">
                <span></span>
                <span></span>
                <span></span>
                </p>
                <p class="crawl">${openingCrawl}</p>
                <ul class="character-list class_${episodeId}">List of Characters:</ul>
                <hr>
                </div>`)
            characters.forEach(characterUrl => {
                fetch(characterUrl)
                    .then(res => res.json())
                    .then(data => {
                        console.log(data)
                        document.querySelector(`.class_${episodeId}`)
                            .innerHTML += `<li class="character">${data.name}</li>`;
                        setTimeout(() =>{
                        const arrLoading = document.querySelectorAll('.loader');
                        arrLoading.forEach(p => p.style = 'display: none')}
                    ,4000)
                    })
            })
        }))
}
getUrlOfFilms('https://ajax.test-danit.com/api/swapi/films')